## Test Practico front-end

Bienvenido al test práctico para aspirantes al área de front-end de Mercado Libre.
A continuación presentamos el diseño y la descripción funcional de una pequeña aplicación que será la base del trabajo
que deberás desarrollar.
La aplicación consta de tres componentes principales: la caja de búsqueda, la visualización de resultados, y la
descripción del detalle del producto.
Tenés que usar el siguiente stack tecnológico para construir la aplicación: <br />
● Cliente:<br />
    1. HTML <br />
    2. JS (Deseable utilizar React o Backbone)<br />
    3. CSS (Deseable utilizar Sass)<br />
● Servidor:<br />
    1. Node >= 6<br />
    2. Express<br />
    
## Para resolverlo, te pedimos que tengas en cuenta: <br />
● Usabilidad <br />
● SEO<br />
● Performance<br />
● Escalabilidad<br />

#Te pedimos: <br />
● En base a los diseños dados, construir las siguientes tres vistas:<br />
    1. Caja de búsqueda<br />
    2. Resultados de la búsqueda<br />
    3. Detalle del producto<br />

● Las vistas son navegables de manera independiente y cuentan con su propia url:<br />
    1. Caja de Búsqueda: ​ “/”<br />
    2. Resultados de la búsqueda:​ “/items?search=?"<br />
    3. Detalle del producto: ​ “/items/:id”<br />

## Tecnologias
1. React
2. Nextjs
3. Node
4. Sass

- Buildeo de la aplicación
`
    npm run build
`
- Start de la Aplicacion
`
    npm run start
`
- url browser 
`
    http:localhost:3000
`