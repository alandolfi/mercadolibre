import React from 'react'

const BreadCrumbItem = ({ value }) => {
  return <span>{ value }</span>
}

export default BreadCrumbItem