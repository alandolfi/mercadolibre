import React from 'react'
import map from 'lodash/map'
import flattenDeep from 'lodash/flattenDeep'
import styles from './breadcrumbs.module.scss'
import BreadCrumbItem from './breadCrumbItem'

const BreadCrumbs = ({ items  }) => {
  return (
    <div className={styles.container}>
      {
        map(items, (item,index) => (
          <>
            <BreadCrumbItem value={item} />
            {              
              (index < (items.length -1)) && <img src="/chevron.png" />
            }
          </>
        ))
      }
    </div>
  )
}

export default BreadCrumbs;
