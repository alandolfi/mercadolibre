import React, { useEffect } from 'react'
import get from 'lodash/get'
import styles from './detail.module.scss'
import Loading from '../loading'
import { resolveCondition, resolveCategoriesDetail } from '../../utils/product'
import BreadCrumbs from '../breadcrumbs'
import { useMeliFetch } from '../../utils/fetch'


const ProductDetail = ({ product, detail}) => {
  const { state:category, onReloadFetch } = useMeliFetch(null)

  useEffect(() => {
    const categoryId = get(product,'data.category_id',null)
    if(categoryId){
      onReloadFetch(`/categories/${categoryId}`)      
    }
  }, [product])

  return (
    <div className={styles.container}>
      <BreadCrumbs items={resolveCategoriesDetail(category)} />
      <section className={styles.containerDetail}>      
        <div className={styles.containerProduct}>
          {
            product.loading ? (<Loading />):
            (
              <>
                <figure className={styles.image}>
                  <img width="680" height="680" src={product.data.pictures[0].url} alt={product.data.title}/>
                </figure>
                <div className={styles.productDetail}>
                  <span className={styles.sold}>{resolveCondition(product.data.condition)} - {product.data.sold_quantity} vendidos</span>
                  <span className={styles.title}>{product.data.title}</span>
                  <span className={styles.price}>$ {product.data.price.toLocaleString()}</span>
                  <button className={styles.button}>Comprar</button>
                </div>
              </>
            )
          }        
        </div>
        <div className={styles.detail}>
          {
            detail.loading ? (<Loading />):
            (
              <>
                <h2>Descripcion del producto</h2>
                <p dangerouslySetInnerHTML={{__html: detail.data.plain_text.replace(/\n/g, '<br />')}}></p>  
              </>
            )
          }
        </div>
      </section>
    </div>
  )
}

export default ProductDetail;
