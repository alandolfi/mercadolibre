import React from 'react'
import SearchBox from '../search'
import styles from './layout.module.scss'

const Layout = (props) => {
  return (
    <div className={styles.container}>
      <SearchBox inputValue={props.inputValue} />
      { props.children }
    </div>
  )
}

export default Layout;