import React from 'react'
import Loading from 'react-loading'

const LoadingC = (props) => {
  return <Loading width={'10%'} height={'10%'} color="blue" type={'spin'}/>
}

export default LoadingC
