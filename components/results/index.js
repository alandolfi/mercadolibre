import React from 'react'
import { useRouter } from 'next/router'
import map from 'lodash/map'
import styles from './products.module.scss'
import ProductItem from './productItem'
import BreadCrumbs from '../breadcrumbs'
import { resolveCategoriesResult } from '../../utils/product'



const Products = ({ product }) => {
  const router = useRouter()

  const onClickProduct = (product) => {
    router.push(`/items/${product.id}`)
  }  

  return(
    <div className={styles.container}>
      <BreadCrumbs items={resolveCategoriesResult(product.category)} />
      <section className={styles.results}>
        {map(product.items,product => ( <ProductItem key={product.id} product={product} onClickProduct={()=> onClickProduct(product)} /> ))}
      </section>
    </div>
  )
}

Products.defaultProps = {
  products: [],
}
export default Products;