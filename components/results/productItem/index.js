import React from 'react'
import styles from './productItem.module.scss'
import get from 'lodash/get'

const Item = (props) => {
  const { product:{ title, thumbnail,price,address}, onClickProduct } = props
  return(
    <>
      <figure className={styles.container} onClick={onClickProduct}>
        <div className={styles.containerImage}>
          <img width="160" height="160" className={styles.image} src={thumbnail} alt={title}/>
        </div>

        <div className={styles.containerDescription}>
          <span>${price.toLocaleString()}</span>
          <p>{title}</p>
        </div>
        
        <div className={styles.containerLocation}>
          <span>{get(address,'state_name','')}</span>
        </div>
      </figure>
      <hr />
    </>
  )
}

Item.defaultProps = {
  product: { },
  onClickItem: () => console.log()
}

export default Item