import React, { useState, useEffect } from 'react'
import styles from './autosuggest.module.scss'

const AutoSuggest = ({placeholder, value, onSearch}) => {
  const [inputValue, setInputValue] = useState(value)

  const onChangeInput= (event) => {
    setInputValue(event.target.value)
  }

  useEffect(() => {
    setInputValue(value)
  }, [value])

  return (
    <div className={styles.container}>      
      <div className={styles.inputSearch}>
          <input name='search' onChange={onChangeInput} value={inputValue} placeholder={placeholder}/>
          <button onClick={() => onSearch(inputValue)}>
            <img src='/ic_Search.png' />
          </button>
      </div>
    </div>
  )
}

AutoSuggest.defaultProps = {
  placeholder: 'Nunca dejes de buscar',
}

export default AutoSuggest