import Layout from './layout'
import AutoSuggest from './autosuggest'
import { useRouter } from 'next/router'
import styles from './layout.module.scss'

export default (props) => {
  const router = useRouter()
  const onSearch=(value) => {
    router.push(`/items?search=${value}`)
  }

  return (
    <Layout>
      <form action="/items" className={styles.form} onSubmit={(e) => e.preventDefault()}>
        <AutoSuggest value={props.inputValue} onSearch={onSearch} />
      </form>
    </Layout>
  )
}