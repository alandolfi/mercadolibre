import React from 'react'
import styles from './layout.module.scss'

const SearchLayout = (props) => {
  return <section className={styles.container}>
    <div>
      <img src="/Logo_ML.png" />
    </div>
    {props.children}
  </section>
}

export default SearchLayout;
