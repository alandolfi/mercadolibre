import React from 'react';
import Layout from '../components/layout'
import Details from '../components/detail'
import { useMeliFetch } from '../utils/fetch'
import { useRouter, withRouter } from "next/router";

const DetailMemoized = React.memo(Details)

const Detail = (props) => {
  const { state:product}  = useMeliFetch(`items/${props.id}`)
  const { state:detail } = useMeliFetch(`items/${props.id}/description`)
  
  const { query: { search }} = useRouter()
  
  return (
    <Layout inputValue={search}>
      <DetailMemoized product={product} detail={detail}/>    
    </Layout>  
  )
}

Detail.getInitialProps = async (ctx) => {
  return { id: ctx.req.params.id }
}

export default withRouter(Detail);
