import React, { useEffect } from 'react'
import { useRouter } from 'next/router'
import Layout from '../components/layout'

export default function Home() {
  const router = useRouter()

  useEffect(() => {
    router.prefetch('/results')
  }, [])

  return (
    <Layout />
  )
}
