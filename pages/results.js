import React, { useEffect, useState } from 'react';
import Layout from '../components/layout'
import Results from '../components/results'
import chunk from 'lodash/chunk'
import { useMeliFetch } from '../utils/fetch'
import { useRouter } from 'next/router'

const ResultMomized = React.memo(Results)

const Result = ({ search }) => {
  const [ product, setProduct] = useState({
    items: [],
    category: []
  })
  const { state:{ data, loading, error}, onReloadFetch } = useMeliFetch(`sites/MLA/search?q=${search}`)
  const router = useRouter()

  useEffect(()=> {
    router.prefetch('/detail')
  })

  useEffect(() => {
    if(search){
      onReloadFetch()
    }
  }, [search])

  useEffect(()=>{
    if(!loading && search){
      const dataPagination = chunk(data.results,4)
      setProduct({
        items:dataPagination[0],
        category: data.filters.find(f => f.id==='category')
      })
    }
  },[data])
  
  return (
    <Layout inputValue={search}>
      <ResultMomized product={product} />    
    </Layout>  
  )
}

Result.getInitialProps = async (ctx) => {
  return { search: ctx.req.query.search }
}

export default Result;
