import { useState, useEffect } from 'react'
const URL_BASE = `https://api.mercadolibre.com/` 

export const useMeliFetch = (relativePath) => {
  const [state, setState] = useState({
    loading:true,
    error:false,
    data:null,
  })

  useEffect(()=> {
    if(relativePath){
      fetchData(relativePath)
    }
  },[])

  const fetchData = async (path) => {
    const response = await fetch(`${URL_BASE}${path}`);
    const data = await response.json()
    setState(prevState => ({
      ...prevState,
      data,
      loading:false
    }))
  }

  const onReloadFetch = (path=relativePath) => {
    setState(prevState => ({
      ...prevState,
      loading:true
    }))
    fetchData(path)
  }

  return { state, onReloadFetch } 
}