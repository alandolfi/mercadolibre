import map from 'lodash/map';
import flattenDeep from 'lodash/flattenDeep';
import get from 'lodash/get'

export const resolveCondition = (type) => {
  return type === 'used' ? 'Usado' : 'Nuevo';
}

export const resolveCategoriesResult = (category) => {
  const categories = flattenDeep(map(category.values, category => (category.path_from_root)))
  const names = map(categories, category => category.name)
  return names;
}

export const resolveCategoriesDetail = (category) => {
  const pathFromRoots = get(category,'data.path_from_root',null)
  if(pathFromRoots){
    return map(pathFromRoots, path => path.name)
  }
  return []
}